@echo off
cd /d %~dp0
set arch=x86 && if "%PROCESSOR_ARCHITECTURE%"=="AMD64" (set arch=x64) else (if "%PROCESSOR_ARCHITEW6432%"=="AMD64" (set arch=x64))
echo Microsoft Visual C++ 2005 Redistributable (VC++ 8.0)
start /wait vcredist-2005-x86.exe /q
if "%arch%" == "x64" start /wait vcredist-2005-x64.exe /q
echo Microsoft Visual C++ 2008 Redistributable (VC++ 9.0)
start /wait vcredist-2008-x86.exe /qb
if "%arch%" == "x64" start /wait vcredist-2008-x64.exe /qb
echo Microsoft Visual C++ 2010 Redistributable (VC++ 10.0)
start /wait vcredist-2010-x86.exe /passive /norestart
if "%arch%" == "x64" start /wait vcredist-2010-x64.exe /passive /norestart
echo Microsoft Visual C++ 2012 Redistributable (VC++ 11.0)
start /wait vcredist-2012-x86.exe /passive /norestart
if "%arch%" == "x64" start /wait vcredist-2012-x64.exe /passive /norestart
echo Microsoft Visual C++ 2013 Redistributable (VC++ 12.0)
start /wait vcredist-2013-x86.exe /passive /norestart
if "%arch%" == "x64" start /wait vcredist-2013-x64.exe /passive /norestart
echo Microsoft Visual C++ 2015, 2017, 2019, 2022 Redistributable (VC++ 14.0)
start /wait vcredist-2015,2017,2019,2022-x86.exe /passive /norestart
if "%arch%" == "x64" start /wait vcredist-2015,2017,2019,2022-x64.exe /passive /norestart
pause
